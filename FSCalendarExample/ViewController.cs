﻿using System;
using Foundation;
using FSCalendar;
using UIKit;

namespace FSCalendarExample
{
	public partial class ViewController : UIViewController, IFSCalendarDataSource, IFSCalendarDelegateAppearance
	{
		protected ViewController(IntPtr handle) : base(handle)
		{
			// Note: this .ctor should not contain any initialization logic.
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			// Perform any additional setup after loading the view, typically from a nib.
			Calendar.DataSource = this;
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}

		[Export("calendar:titleForDate:")]
		public string CalendarTitleForDate(FSCalendar.FSCalendar calendar, NSDate date)
		{
			if (calendar.Gregorian().IsDateInToday(date)) {
				return "今";
			}

			return null;
		}
	}

}
