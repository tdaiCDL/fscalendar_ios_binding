﻿using System;
using System.Runtime.InteropServices;
using ObjCRuntime;

namespace FSCalendar
{
	[Native]
	public enum FSCalendarCellState : ulong
	{
		Normal = 0,
		Selected = 1,
		Placeholder = 1 << 1,
		Disabled = 1 << 2,
		Today = 1 << 3,
		Weekend = 1 << 4,
		TodaySelected = Today | Selected
	}

	[Native]
	public enum FSCalendarSeparators : ulong
	{
		None = 0,
		InterRows = 1
	}

	[Native]
	public enum FSCalendarCaseOptions : ulong
	{
		HeaderUsesDefaultCase = 0,
		HeaderUsesUpperCase = 1,
		WeekdayUsesDefaultCase = 0 << 4,
		WeekdayUsesUpperCase = 1 << 4,
		WeekdayUsesSingleUpperCase = 2 << 4
	}

	[Native]
	public enum FSCalendarScope : ulong
	{
		Month,
		Week
	}

	[Native]
	public enum FSCalendarScrollDirection : ulong
	{
		Vertical,
		Horizontal
	}

	[Native]
	public enum FSCalendarPlaceholderType : ulong
	{
		None = 0,
		FillHeadTail = 1,
		FillSixRows = 2
	}

	[Native]
	public enum FSCalendarMonthPosition : ulong
	{
		Previous,
		Current,
		Next,
		NotFound
	}

	[StructLayout(LayoutKind.Sequential)]
	public struct FSCalendarCoordinate
	{
		public nint row;

		public nint column;
	}

	[Native]
	public enum FSCalendarTransitionState : ulong
	{
		Idle,
		Changing,
		Finishing
	}

}
